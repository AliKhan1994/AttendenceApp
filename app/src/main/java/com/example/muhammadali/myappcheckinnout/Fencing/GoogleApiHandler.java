package com.example.muhammadali.myappcheckinnout.Fencing;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

public class GoogleApiHandler {

    private static GoogleApiHandler handler;

    private Context context;

    private GoogleApiClient googleApiClient;

    private Location lastLocation;

    public static GoogleApiHandler getInstance(Context context) {
        return handler != null ? handler : (handler = new GoogleApiHandler(context));
    }

    private GoogleApiHandler(Context context) {
        this.context = context;
        setUpPlayService();
    }

    private void setUpPlayService() {
        GoogleApiClient.ConnectionCallbacks callbacks = new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {

            }

            @Override
            public void onConnectionSuspended(int i) {

            }
        };
        GoogleApiClient.OnConnectionFailedListener listener = new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

            }
        };
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(callbacks)
                    .addOnConnectionFailedListener(listener)
                    .addApi(LocationServices.API)
                    .build();
        }
        googleApiClient.connect();
    }

    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }
}