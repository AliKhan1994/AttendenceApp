package com.example.muhammadali.myappcheckinnout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.muhammadali.myappcheckinnout.database.DbScripts;

public class AddFences extends AppCompatActivity {

    EditText eTfenceId, eTfenceRadius, eTfenceLat, eTfenceLan;
    Button btnFenceAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_fences);

        eTfenceId = (EditText) findViewById(R.id.eTfenceID);
        eTfenceRadius = (EditText) findViewById(R.id.eTfenceRadius);
        eTfenceLat = (EditText) findViewById(R.id.eTfenceLat);
        eTfenceLan = (EditText) findViewById(R.id.eTfenceLan);

        btnFenceAdd = (Button) findViewById(R.id.btnfenceAdd);

        btnFenceAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int radius = Integer.parseInt(eTfenceRadius.getText().toString());
                double lat = Double.parseDouble(eTfenceLat.getText().toString());
                double lan = Double.parseDouble(eTfenceLan.getText().toString());


                long id = DbScripts.insertData(radius, lat, lan);
                if (id != -1) {
                    Toast.makeText(getApplicationContext(), "data Added to database", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "data NOT Added to database", Toast.LENGTH_SHORT).show();

                }
                fence fence = new fence(id, radius, lat, lan);


                Intent intent = new Intent();

                intent.putExtra("fence", fence);
                setResult(RESULT_OK, intent);
                finish();

            }
        });


    }
}
