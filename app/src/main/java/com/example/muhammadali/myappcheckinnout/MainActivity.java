package com.example.muhammadali.myappcheckinnout;

import android.*;
import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muhammadali.myappcheckinnout.Fencing.FencingHandler;
import com.example.muhammadali.myappcheckinnout.database.DbScripts;
import com.example.muhammadali.myappcheckinnout.util.AppLog;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    ArrayList<SingleRow> list;


    Button chkInBtn, chkOutBtn, btnAddFence;
    TextView statusTxt;

//    EditText editTextId,editTextRadius,editTextLat,editTextLan;

    String s;
    ListView listView;
    SharedPreferences pref;
    SharedPreferences.Editor editor;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE); // 0 - for private mode
        editor = pref.edit();


        chkInBtn = (Button) findViewById(R.id.checkInBtn);
        chkOutBtn = (Button) findViewById(R.id.checkOutBtn);
        statusTxt = (TextView) findViewById(R.id.statusTxt);

//        btnUpdate = (Button) findViewById(R.id.btnUpdateFence);
//        btnDelete = (Button) findViewById(R.id.btnDeleteFence);
//        editTextId= (EditText)findViewById(R.id.eTfenceId);
//        editTextRadius= (EditText)findViewById(R.id.eTfenceRadius);
//        editTextLat= (EditText)findViewById(R.id.eTfenceLan);
//        editTextLan= (EditText)findViewById(R.id.eTfenceLan);


        listView = (ListView) findViewById(R.id.fenceList);
        refreshFences();

        btnAddFence =   (Button) findViewById(R.id.btnAddFence);

        boolean s   =   pref.getBoolean("status", false);

        if (s == false) {

            statusTxt.setText("Checked out");

        } else if (s == true) {

            statusTxt.setText("Checked IN");
        }


        chkInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                boolean s = pref.getBoolean("status", false);

                if (s == false) {

                    editor.putBoolean("status", true); // Storing boolean - true/false
                    editor.commit();
                    statusTxt.setText("Checked IN");

                } else if (s == true) {

                    Toast.makeText(getApplicationContext(), "User Already Checked IN!!", Toast.LENGTH_SHORT).show();
                }
            }


        });

        chkOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean s = pref.getBoolean("status", false);
                if (s == true) {

                    editor.putBoolean("status", false); // Storing boolean - true/false
                    editor.commit();
                    statusTxt.setText("Checked OUT");

                } else if (s == false) {

                    Toast.makeText(getApplicationContext(), "First Check IN to Check OUT !!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnAddFence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(intent);

            }
        });


//        btnUpdate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
////                String idUpdate=editTextId.getText().toString();
////                String radiusUpdate=editTextRadius.getText().toString();
////                String latUpdate=editTextLat.getText().toString();
////                String lanUpdate=editTextLan.getText().toString();
//
//                boolean isUpdate = myDb.updateData(idUpdate,radiusUpdate,latUpdate,lanUpdate);
//
//                if(isUpdate == true){
//
//                    Toast.makeText(getApplicationContext(),"Data Updated",Toast.LENGTH_SHORT).show();
//                    refreshFences();
//
//                }else{
//
//                    Toast.makeText(getApplicationContext(),"Data Not Updated",Toast.LENGTH_SHORT).show();
//
//
//                }
//
//
//
//            }
//        });


//
//        btnDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//  /*              Intent intent = new Intent(getApplicationContext(), AddFences.class);
//                startActivityForResult(intent, 1);
//
//*/
//
//            }
//        });


    }

    void refreshFences() {

        list = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            list.clear();
            listView.setAdapter(new CustomAdapter(this, list));

            return;

        }

        Cursor res = DbScripts.getAllData();
        if (res.getCount() == 0) {

            Toast.makeText(this, "No ROw in database", Toast.LENGTH_SHORT).show();
            return;

        }

        list = new ArrayList<>();                    //making object of array list
        ArrayList<FencingHandler.FenceInfo> listRegisterFence = new ArrayList<>();
        listRegisterFence.clear();



        while (res.moveToNext()) {

            list.add(new SingleRow(res.getInt(0), res.getInt(1), res.getDouble(2), res.getDouble(3)));
            FencingHandler.FenceInfo fenceInfo = new FencingHandler.FenceInfo();

            fenceInfo.id = res.getInt(0);
            fenceInfo.radius = res.getInt(1);
            fenceInfo.latitude = res.getDouble(2);
            fenceInfo.longitude = res.getDouble(3);
            AppLog.i(TAG, "refresh list --- fence info --- "+ fenceInfo);

            listRegisterFence.add(fenceInfo);

        }

        listView.setAdapter(new CustomAdapter(this, list));
        AppLog.i(TAG,"list of fences size = " + listRegisterFence.size());

        if (listRegisterFence.size() == 0){

            return;

        }

        FencingHandler handler = new FencingHandler(getApplicationContext(), listRegisterFence);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean perm = false;
        for (int i = 0; i < permissions.length; i++) {

            String s = permissions[i];
            int j = grantResults[i];
            if (s.equals(Manifest.permission.RECORD_AUDIO)) {

                perm = j == PackageManager.PERMISSION_GRANTED;

            }
        }

        if (requestCode == 0 && perm) {

            Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
            refreshFences();

        }

    }


    @Override
    protected void onResume() {
        refreshFences();
        super.onResume();

    }
}
