package com.example.muhammadali.myappcheckinnout;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.muhammadali.myappcheckinnout.database.DbScripts;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class FenceUpdateActivity extends FragmentActivity implements OnMapReadyCallback {


    private GoogleMap mMap;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    SeekBar seekBar;
    LatLng latLngDB;


    int previousFenceRadius,previousFenceID;
    double previousFenceLat,previousFenceLan;


    int updateFenceRadius;
    double updateFenceLat,updateFenceLan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_fence);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_for_update);
        mapFragment.getMapAsync(this);


        //Get the bundle
        Bundle bundle = getIntent().getExtras();

//Extract the data…


        previousFenceID     = bundle.getInt("updateFenceID");

        previousFenceRadius =   bundle.getInt("updateFenceRadius");
        updateFenceRadius=previousFenceRadius;

        previousFenceLat    =   bundle.getDouble("updateFenceLat");
        previousFenceLan    =   bundle.getDouble("updateFenceLan");



        pref = getSharedPreferences("MyPref",MODE_PRIVATE); // 0 - for private mode
        editor = pref.edit();


        seekBar = (SeekBar) findViewById(R.id.seek_bar);

        Button button = (Button) findViewById(R.id.updateFence);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( latLngDB == null ){
                    Toast.makeText(getApplicationContext(),"Select Location",Toast.LENGTH_SHORT).show();
                    return;
                }else{





                   Toast.makeText(getApplicationContext(),"Location Selected",Toast.LENGTH_SHORT).show();
/*                    Toast.makeText(getApplicationContext(),"Radius : "+updateFenceRadius,Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(),"Lat :"+updateFenceLat+"  Lng : "+updateFenceLan,Toast.LENGTH_SHORT).show();

*/

                    int checkedInFenceId = pref.getInt("id",-1);

                    if(checkedInFenceId == previousFenceID) {
                        editor.putBoolean("status", false);
                        editor.commit();
                        editor.putInt("id", -1);
                        editor.commit();
                    }



                    boolean isUpdate = DbScripts.updateData(previousFenceID,updateFenceRadius,updateFenceLat,updateFenceLan);

                    if(isUpdate == true){

                        Toast.makeText(getApplicationContext(),"Data Updated",Toast.LENGTH_SHORT).show();
                      //  refreshFences();

                    }else{

                        Toast.makeText(getApplicationContext(),"Data Not Updated",Toast.LENGTH_SHORT).show();


                    }



                    finish();




                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat
                .checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat
                        .checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.setMyLocationEnabled(true);

        // Add a marker in Sydney and move the camera
        LatLng previousFenceLocation= new LatLng(previousFenceLat,previousFenceLan);

        final Marker marker = mMap.addMarker(new MarkerOptions().position(previousFenceLocation).title("Current Fence"));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(previousFenceLocation));
        final Circle circle = mMap.addCircle(new CircleOptions()
                .center(previousFenceLocation)
                .fillColor(0xaaaaaaaa)
                .radius(previousFenceRadius)
        );


        mMap.animateCamera(CameraUpdateFactory.newLatLng(previousFenceLocation), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {

            }

            @Override
            public void onCancel() {

            }
        });


        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                latLngDB=latLng;

                updateFenceLat=latLng.latitude;
                updateFenceLan=latLng.longitude;

                MarkerOptions markerOptions = new MarkerOptions();

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
//               markerOptions.title(latLng.latitude + " : " + latLng.longitude);

                marker.setPosition(latLngDB);
                marker.setTitle(latLngDB.latitude + " : " + latLngDB.longitude);
                circle.setCenter(latLngDB);
            }
        });

        seekBar.setMax(500);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                circle.setRadius(i);
                updateFenceRadius = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }
}
