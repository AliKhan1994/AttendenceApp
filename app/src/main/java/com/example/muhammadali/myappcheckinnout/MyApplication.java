package com.example.muhammadali.myappcheckinnout;

import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import com.example.muhammadali.myappcheckinnout.database.DbHandler;

/**
 * Created by Amir Raza on 12/23/2016.
 */

public class MyApplication extends android.app.Application {

    private static final String  TAG = "IWhisperApplication";

    private static MyApplication context;

    public static MyApplication getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        if(checkPermissionWriteExternal()) {
            DbHandler.getInstance();
        }
    }

    public static boolean checkPermissionWriteExternal(){
        int writePerm;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            try {
                writePerm = ContextCompat.checkSelfPermission(MyApplication.getContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            } catch (Exception e){
                e.printStackTrace();
                return false;
            }
            return writePerm == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }
}
