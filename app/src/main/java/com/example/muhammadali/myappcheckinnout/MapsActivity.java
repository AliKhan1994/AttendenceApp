package com.example.muhammadali.myappcheckinnout;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Toast;

import com.example.muhammadali.myappcheckinnout.database.DbScripts;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    EditText eTMapFenceId;
    SeekBar seekBar;
    int radiusDB = 50;
    LatLng latLngDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        eTMapFenceId = (EditText) findViewById(R.id.eTMapFenceID);
        seekBar = (SeekBar) findViewById(R.id.seek_bar);

        Button button = (Button) findViewById(R.id.register);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                    if (latLngDB == null) {
                        Toast.makeText(getApplicationContext(), "Select Location", Toast.LENGTH_SHORT).show();
                        return;

                    } else {


                        Toast.makeText(getApplicationContext(), "Location Selected", Toast.LENGTH_SHORT).show();

                    /*

                    Toast.makeText(getApplicationContext(),"Radius : "+radiusDB,Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(),"Lat :"+latLngDB.latitude+"  Lng : "+latLngDB.longitude,Toast.LENGTH_SHORT).show();

                    */


                        long id = DbScripts.insertData(radiusDB, latLngDB.latitude, latLngDB.longitude);
                        if (id != -1) {
                            Toast.makeText(getApplicationContext(), "data Added to database", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "data NOT Added to database", Toast.LENGTH_SHORT).show();
                        }

                        finish();
                    }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat
                .checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat
                        .checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.setMyLocationEnabled(true);

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(24.9333, 67.0333);

        final Marker marker = mMap.addMarker(new MarkerOptions().position(sydney).title("seydney"));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        final Circle circle = mMap.addCircle(new CircleOptions()
                .center(sydney)
                .fillColor(0xaaaaaaaa)
                .radius(radiusDB)
        );


        mMap.animateCamera(CameraUpdateFactory.newLatLng(sydney), new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {

            }

            @Override
            public void onCancel() {

            }
        });


        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                latLngDB = latLng;
                MarkerOptions markerOptions = new MarkerOptions();

                // Setting the position for the marker
                markerOptions.position(latLng);

                // Setting the title for the marker.
                // This will be displayed on taping the marker
//               markerOptions.title(latLng.latitude + " : " + latLng.longitude);

                marker.setPosition(latLng);
                marker.setTitle(latLng.latitude + " : " + latLng.longitude);
                circle.setCenter(latLng);
                latLngDB = latLng;
            }
        });

        seekBar.setMax(500);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                circle.setRadius(i);

                radiusDB = i;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }
}
