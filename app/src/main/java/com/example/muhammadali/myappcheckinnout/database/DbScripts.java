package com.example.muhammadali.myappcheckinnout.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.muhammadali.myappcheckinnout.MyApplication;
import com.example.muhammadali.myappcheckinnout.util.AppLog;

/**
 * Created by Muhammad Faizan Khan on 7/19/2016.
 */
public class DbScripts {

    private static final String TAG = DbScripts.class.getSimpleName();


    private static SQLiteDatabase database;

    private static final String TABLE_NAME = "fence_table";
    private static final String COL_1 = "ID";
    private static final String COL_2 = "RADIUS";
    private static final String COL_3 = "LAT";
    private static final String COL_4 = "LAN";

    static {
        if (MyApplication.checkPermissionWriteExternal()) {
            getDatabase();
        }
    }

    private static void getDatabase() {
        DbHandler dbHandler = DbHandler.getInstance();
        dbHandler.openDataBase();
        database = dbHandler.getDatabase();
    }

    public static long insertData(int radius, double lat, double lan) {

        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_2, radius);
        contentValues.put(COL_3, lat);
        contentValues.put(COL_4, lan);


        long result = database.insert(TABLE_NAME, null, contentValues);
        AppLog.i(TAG,"insertData -- id = " + result);
        return result;
    }

    public static boolean updateData(int id, int radius, double lat, double lan) {
        if (database == null || !database.isOpen()) {
            getDatabase();
        }

        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_2, radius);
        contentValues.put(COL_3, lat);
        contentValues.put(COL_4, lan);

        database.update(TABLE_NAME, contentValues, COL_1 + "=" + id, null);

        return true;


    }

    public static Cursor getAllData() {
        if (database == null || !database.isOpen()) {
            getDatabase();
        }
        Cursor res = database.rawQuery("select * from " + TABLE_NAME, null);

        return res;
    }

    public static Integer deleteData(int id) {
        Log.v("Hello", "In deleteData" + id);
        if (database == null || !database.isOpen()) {
            getDatabase();
        }

        return database.delete(TABLE_NAME, "ID=" + id, null);
    }

}
