package com.example.muhammadali.myappcheckinnout.Fencing;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.transition.Transition;
import android.util.Log;

import com.example.muhammadali.myappcheckinnout.fence;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.GeofencingRequest;

import java.util.ArrayList;

public class GeofenceTransitionsIntentService extends IntentService {




    SharedPreferences pref;
    SharedPreferences.Editor editor;


    int geofenceTransitionType;


    private static final String TAG = GeofenceTransitionsIntentService.class.getSimpleName();

    public GeofenceTransitionsIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        Log.e(TAG, "test");

        if (geofencingEvent == null) {
            return;
        }
        if (geofencingEvent.hasError()) {
            //In Case Of Any Error
//            String errorMessage = getErrorString(geofencingEvent.getErrorCode());
//            Log.e(TAG, errorMessage);
            return;
        }
        Log.e(TAG, "test");

        // Get the transition type.
        geofenceTransitionType = geofencingEvent.getGeofenceTransition();   // fence triger type

        ArrayList<Geofence> triggeringGeoFences = (ArrayList<Geofence>) geofencingEvent.getTriggeringGeofences();

                /// yaha ek list banlooo all fences ke..   user ke state....check inn checkout

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE); // 0 - for private mode
        editor = pref.edit();

        boolean checkedId = pref.getBoolean("status",false);
        int checkedInFenceId = pref.getInt("id",-1);

        if (checkedId){
            if (!(geofenceTransitionType == GeofencingRequest.INITIAL_TRIGGER_EXIT)){
                return;
            } else{
                for (Geofence geofence :
                        triggeringGeoFences) {

                    String s = geofence.getRequestId();
                    int id = Integer.parseInt(s);
                    if (id == checkedInFenceId){
                        //exit
                        //change status
                        //id null

                        editor.putBoolean("status",false);
                        editor.commit();

                        editor.putInt("id",-1);
                        editor.commit();

                        String msg= "Exit from fence : "+id;
                        generateNotification("fencing notification",msg );

                    }
                }

            }
        } else {
            if (!( geofenceTransitionType == GeofencingRequest.INITIAL_TRIGGER_DWELL )){
                return;
            } else{

                Geofence geofence = triggeringGeoFences.get(0);
                String s = geofence.getRequestId();
                int id = Integer.parseInt(s);

                //enter
                //change status
                //id set
                editor.putBoolean("status",true);
                editor.commit();

                editor.putInt("id",id);
                editor.commit();

                String msg= "Entered in fence : "+id;
                generateNotification("fencing notification",msg );


            }
        }

/*
        editor = pref.edit();

        String geofenceTransitionDetails = getGeofenceTransitionDetails( this , geofenceTransitionType , triggeringGeoFences);
*/


/*
        if(geofenceTransitionType == GeofencingRequest.INITIAL_TRIGGER_DWELL){



           editor.putBoolean("status",true);

            editor.commit();


        }else if(geofenceTransitionType == GeofencingRequest.INITIAL_TRIGGER_EXIT){

            editor.putBoolean("status",false);

            editor.commit();

        }
*/



        // Test that the reported transition was not of interest.


//     logTransitionType(geofenceTransitionType);
        
//        String msg = getMessgeFromGeofences(triggeringGeoFences);


    }

//    private String getGeofenceTransitionDetails(Context context,int geofenceTransition, ArrayList<Geofence> triggeringGeofences) {
//
//        String geofenceTransitionString = getTransitionString(geofenceTransition);
//
//        // Get the Ids of each geofence that was triggered.
//        ArrayList triggeringGeofencesIdsList = new ArrayList();
//        for (Geofence geofence : triggeringGeofences) {
//            triggeringGeofencesIdsList.add(geofence.getRequestId());
//        }
//        String triggeringGeofencesIdsString = TextUtils.join(",",triggeringGeofencesIdsList);
//
//
//
//        return geofenceTransitionString + ": " + triggeringGeofencesIdsString;
//    }



    private String getTransitionString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_DWELL:
                return "Entered in ";
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return "Exit from ";
            default:
                return "unknown";
        }
    }



//        private String getMessgeFromGeofences(ArrayList<Geofence> triggeringGeoFences){
//        //return meaningful string for the triggered fence
//
//        if(geofenceTransitionType == GeofencingRequest.INITIAL_TRIGGER_DWELL ){
//
//            return "Entered in fence";
//
//        }else {                                                                  // if Geo fence triger exit notification
//
//            return "Exit from fence";
//
//        }
//
//    }
    
    private void generateNotification(String title, String description) {
        Log.i(TAG, "generating notificeation");

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)

                //one or both are mandatory for notification
                .setContentTitle(title)
                .setContentText(description)

//                .setTicker("")//what appeares at the status bar

                //icons are required otherwise notification will not be shown
                .setSmallIcon(android.R.drawable.ic_menu_mylocation)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), android.R.drawable.ic_menu_mylocation))
                .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND);
                //following line for opening activity on taping notification
                // .setContentIntent(PendingIntent.getActivity(this, 1, new Intent(this, HomeActivity.class), PendingIntent.FLAG_UPDATE_CURRENT))

        Notification notification = notificationBuilder.build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(
            //id at which this notification will be triggered
            //if previous notification is already being shown then this one will override the previous one
            1,
        notification);
    }


//    private void logTransitionType(int geofenceTransitionType) {
//        switch (geofenceTransitionType) {
//            case Geofence.GEOFENCE_TRANSITION_ENTER:
//                generateNotification("fencing notification","Enter Occur");
//                // AppLog the error.
//                Log.e(TAG, "GEOFENCE_TRANSITION_ENTER occure");
//                break;
//            case Geofence.GEOFENCE_TRANSITION_DWELL:
//
//                generateNotification("fencing notification","Dwell Occur");
//
//                Log.e(TAG, "GEOFENCE_TRANSITION_ENTER occure");
//                break;
//            case Geofence.GEOFENCE_TRANSITION_EXIT:
//
//                generateNotification("fencing notification","Exit Occur");
//
//                Log.e(TAG, "GEOFENCE_TRANSITION_ENTER occure");
//                break;
//        }
//    }
}
