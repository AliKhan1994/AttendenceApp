package com.example.muhammadali.myappcheckinnout;

import java.io.Serializable;

/**
 * Created by Zain on 11/23/2016.
 */

public class fence implements Serializable {

    long id;
    int radius;
    double lat, lan;


    public fence(long id, int radius, double lat, double lan) {
        this.id = id;
        this.radius = radius;
        this.lat = lat;
        this.lan = lan;


    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(long lat) {
        this.lat = lat;
    }

    public double getLang() {
        return lan;
    }

    public void setLang(long lang) {
        this.lan = lang;
    }
}
