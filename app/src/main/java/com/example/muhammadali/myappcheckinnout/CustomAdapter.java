package com.example.muhammadali.myappcheckinnout;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.muhammadali.myappcheckinnout.Fencing.GoogleApiHandler;
import com.example.muhammadali.myappcheckinnout.database.DbScripts;
import com.google.android.gms.location.GeofencingApi;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

/**
 * Created by MuhammadAli on 20-Oct-16.
 */
public class CustomAdapter extends BaseAdapter implements DialogInterface.OnClickListener {


    SharedPreferences pref;
    SharedPreferences.Editor editor;

    String[] dialogOption = {"UPDATE", "DELETE"};
    ArrayList<SingleRow> list;
    Context context;
    int selectedID;
    int selectedRadius;
    double selectedLat;
    double selectedLan;


    AlertDialog ad;

    CustomAdapter(Context context, ArrayList<SingleRow> list) {

        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.single_row_fence_list, viewGroup, false);

        pref = context.getSharedPreferences("MyPref", context.MODE_PRIVATE); // 0 - for private mode
        editor = pref.edit();


        TextView id = (TextView) row.findViewById(R.id.fenceId);
        TextView radius = (TextView) row.findViewById(R.id.fenceRadius);
        TextView lat = (TextView) row.findViewById(R.id.fenceLat);
        TextView lan = (TextView) row.findViewById(R.id.fenceLan);
        Button dialogbtn = (Button) row.findViewById(R.id.dialogBtn);
        dialogbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedID = list.get(i).Id;
                selectedRadius = list.get(i).Radius;
                selectedLat = list.get(i).lat;
                selectedLan = list.get(i).lan;


                ad.show();
            }
        });

        SingleRow temp = list.get(i);


        id.setText(String.format("%d", temp.Id));
        radius.setText(String.format("%d", temp.Radius));
        lat.setText(String.format("%.4f", temp.lat));
        lan.setText(String.format("%.4f", temp.lan));


        // Building Aleart Box
        ///////////////////////////////////////////////////////////////
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Action");
        builder.setItems(dialogOption, this);
        //for cancelation
        builder.setNegativeButton("Cancel", null);
        ad = builder.create();
        //////////////////////////////////////////////////////////////
        return row;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int position) {
        String selectedItem = dialogOption[position];
        if (selectedItem == "UPDATE") {
            Toast.makeText(context, "Update", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(context, FenceUpdateActivity.class);
            Bundle bundle = new Bundle();

//Add your data to bundle
            bundle.putInt("updateFenceID", selectedID);
            bundle.putInt("updateFenceRadius", selectedRadius);
            bundle.putDouble("updateFenceLat", selectedLat);
            bundle.putDouble("updateFenceLan", selectedLan);

//Add the bundle to the intent
            intent.putExtras(bundle);
//Fire that second activity
            context.startActivity(intent);

        } else if (selectedItem == "DELETE") {


            Toast.makeText(context, "Delete", Toast.LENGTH_SHORT).show();
            Log.v("ALI", "In delete" + selectedID);


            int checkedInFenceId = pref.getInt("id", -1);

            if (checkedInFenceId == selectedID) {
                editor.putBoolean("status", false);
                editor.commit();
                editor.putInt("id", -1);
                editor.commit();
            }

//            ArrayList<String> removeFence=new ArrayList<String>();
//
//            for (int i=1 ; i<=selectedID ; i++){
//
//                removeFence.add(""+i);
//
//            }
//
//            LocationServices.GeofencingApi.removeGeofences(  GoogleApiHandler.getInstance(context).getGoogleApiClient(),removeFence);
            Integer deletedRows = DbScripts.deleteData(selectedID);


            if (deletedRows > 0) {
                Toast.makeText(context, "fence Deleted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "fence Not Deleted", Toast.LENGTH_SHORT).show();
            }
        }
    }
}


